package com.games.msspaceinvaders.models;

import lombok.Data;

@Data
public class Enemies{


	private int xAxis;

	private int yAxis;

	private boolean isAlive;


	public Enemies(int xAxis, int yAxis, boolean isAlive){

		this.xAxis = xAxis;
		this.yAxis = yAxis;
		this.isAlive = isAlive;

	}

	public void moveLeft(){

		if(this.xAxis - 10 > 0){

			this.xAxis-=10;

		}
		
	}

	public void moveDown(){

		this.yAxis+=10;

	}


	public boolean isDead(Bullet agentBullet){

		// TODO:

		return isAlive;
	
	}


}