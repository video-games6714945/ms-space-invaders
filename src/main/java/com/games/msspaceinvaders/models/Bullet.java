package com.games.msspaceinvaders.models;

import lombok.Data;

@Data
public class Bullet{


	private final int xAxis;

	private int yAxis;

	public Bullet(int xAxis, int yAxis){

		this.xAxis = xAxis;
		this.yAxis = yAxis;
		
	}


	public void moveUp(){

		this.yAxis-=10;
	
	}


	public void moveDown(){

		this.yAxis+=10;

	}


}