package com.games.msspaceinvaders.models;

// Internal dependencies:

import com.games.msspaceinvaders.constants.AgentConstants;


// External dependencies:

import lombok.Data;

import java.util.List;

@Data
public class Agent{


	private int xAxis;

	private final int yAxis;

	private boolean isAlive;


	public Agent(int xAxis, boolean isAlive){

		this.xAxis = xAxis;
		this.yAxis = AgentConstants.Y_AXIS.value;
		this.isAlive = isAlive;

	}

	// On left click, the agent moves leftwards on the screen:

	public void leftClick(){

		if(this.xAxis - 10 > 0){

			this.xAxis-=10;

		}
		
	}


	// On right click, the agent moves rightwards on the screen:

	public void rightClick(){

		if(this.xAxis - 10 < 100){

			this.xAxis+=10;

		}
		
	}

	// Validate if agent is within the bullet area

	public boolean isDead(List<Bullet> enemyBullets){

		// TODO --> position of bullet with reference to Agent

		return isAlive;
	
	}

}
