package com.games.msspaceinvaders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class MsSpaceInvadersApplication {

	public static void main(String[] args) {
		
		SpringApplicationBuilder builder = new SpringApplicationBuilder(MsSpaceInvadersApplication.class);
		builder.headless(false);

		ConfigurableApplicationContext context = builder.run(args);

	}

}
