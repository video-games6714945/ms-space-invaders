package com.games.msspaceinvaders.sound;


// External dependencies:

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.core.io.Resource;



import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.Clip;
import java.io.IOException;


@Service
public class SoundPlayer implements LineListener {



    public SoundPlayer(@Value("classpath:music/background.wav") Resource backgroundSound)throws UnsupportedAudioFileException,
        LineUnavailableException, IOException{

        AudioInputStream audioStream = AudioSystem.getAudioInputStream(backgroundSound.getInputStream());

        DataLine.Info info = new DataLine.Info(Clip.class, audioStream.getFormat());

        Clip audioClip = (Clip) AudioSystem.getLine(info);
        audioClip.addLineListener(this);
        audioClip.open(audioStream);
        audioClip.loop(Clip.LOOP_CONTINUOUSLY);
    
    }


    @Override
    public void update(LineEvent event) {}

}