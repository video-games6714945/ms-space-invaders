package com.games.msspaceinvaders.configs;


// Internal dependencies:

import com.games.msspaceinvaders.configs.properties.EnemiesConfigs;
import com.games.msspaceinvaders.configs.properties.AgentConfigs;
import com.games.msspaceinvaders.listeners.AgentMovements;
import com.games.msspaceinvaders.displays.GameDisplay;
import com.games.msspaceinvaders.models.Enemies;
import com.games.msspaceinvaders.models.Bullet;
import com.games.msspaceinvaders.models.Agent;

// External dependencies:

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;


import java.util.ArrayList;
import java.util.List;


@Configuration
@EnableScheduling
public class Beans{
 

    private final EnemiesConfigs enemiesConfigs;

    private final AgentConfigs agentConfigs;

    
    public Beans(EnemiesConfigs enemiesConfigs, AgentConfigs agentConfigs){

        this.enemiesConfigs = enemiesConfigs;
        this.agentConfigs = agentConfigs;

    }


    // Create the agent that user will control:
    
    @Bean
    public Agent createAgent() {
    
        return new Agent(agentConfigs.getXAxis(), agentConfigs.getIsAlive());
    
    }


    // Create a list of enemies that user will fight:

    // @Bean
    // public Enemies createEnemies() {
    
    //     return new Enemies(agentConfigs.getXAxis(), agentConfigs.getYAxis(), agentConfigs.getIsAlive());

    // }


    // 

    // @Bean
    // public Bullet createAgentBullet() {
    
    //     return new Bullet();

    // }


    // 

    // @Bean
    // public List<Bullet> createEnemyBullets() {
    
    //     return new ArrayList<>();
    
    // }





}
        