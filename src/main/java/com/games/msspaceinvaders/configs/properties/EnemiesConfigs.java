package com.games.msspaceinvaders.configs.properties;

// External dependencies:

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import lombok.Data;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties("msspaceinvaders.enemies")
public class EnemiesConfigs {

    private List<Integer> xAxis;

    private List<Integer> yAxis;
    
    private List<Boolean> isAlive;

}