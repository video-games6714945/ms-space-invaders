package com.games.msspaceinvaders.configs.properties;

// External dependencies:

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties("msspaceinvaders.agent")
public class AgentConfigs {

    private int xAxis;
    
    private Boolean isAlive;

}