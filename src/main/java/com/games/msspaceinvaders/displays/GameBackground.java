package com.games.msspaceinvaders.displays;

// External dependencies:

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.core.io.Resource;


import javax.swing.JComponent;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.awt.Graphics;
import java.awt.Image;


@Service
public class GameBackground extends JComponent{

    private final Image image;

    @Autowired
    public GameBackground(@Value("classpath:images/background.jpg") Resource backgroundImage) throws IOException{
 
        image = ImageIO.read(backgroundImage.getInputStream());

    }

    @Override
    protected void paintComponent(Graphics g) {
    
        super.paintComponent(g);
        g.drawImage(image, 0, 0, this);
    
    }

}

