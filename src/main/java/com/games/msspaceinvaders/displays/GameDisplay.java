package com.games.msspaceinvaders.displays;

// Internal depedencies:

import com.games.msspaceinvaders.listeners.AgentMovements;

// External dependencies:

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.JFrame;
import java.awt.Dimension;


@Service
public class GameDisplay extends JFrame{

	@Autowired
	public GameDisplay(AgentMovements agentMovements, GameBackground background){

        setTitle("ＳＰＡＣＥ ＩＮＶＡＤＥＲＳ");
        setResizable(false);
        setSize(600, 600);
        setMinimumSize(new Dimension(600, 600));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setContentPane(background);
        pack();
        setVisible(true);
	    addKeyListener(agentMovements);
	    setFocusable(true);
	    setFocusTraversalKeysEnabled(false);   

	}


}


