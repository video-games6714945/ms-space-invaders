package com.games.msspaceinvaders.constants;

public enum AgentConstants{
 
    Y_AXIS(450),
    MIN_XAXIS(10),
    MAX_XAXIS(490);

    public final int value;

    AgentConstants(int value){

        this.value = value;
    }

}