package com.games.msspaceinvaders.listeners;

// Internal dependencies:

import com.games.msspaceinvaders.constants.AgentConstants;


// External dependencies:

import org.springframework.stereotype.Service;
import com.games.msspaceinvaders.models.Agent;


import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;


@Service
public class AgentMovements implements KeyListener{

    private final Agent agent;

    public AgentMovements(Agent agent){
        
        this.agent = agent;

    }


    // On left click, the agent moves x distance left and on right click, the agent moves x distance right with respect to limitations:

    public void keyPressed(KeyEvent e) {
        
        switch (e.getExtendedKeyCode()) {
            case 37:
                agent.setXAxis(((agent.getXAxis() - 10) < AgentConstants.Y_AXIS.value) ? AgentConstants.MIN_XAXIS.value : 
                    agent.getXAxis() - 10 );
                break;
            case 39:
                agent.setXAxis(((agent.getXAxis() + 10) > AgentConstants.MAX_XAXIS.value) ? AgentConstants.MAX_XAXIS.value : 
                    agent.getXAxis() + 10 );
                break;
        }

    }

    public void keyReleased(KeyEvent e) {

    }
    
    public void keyTyped(KeyEvent e) {
    
    }

}